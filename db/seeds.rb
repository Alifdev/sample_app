# Users
User.create!(name:  "Alina User",
             email: "alina@rails.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)
99.times do |n|
  name  = Faker::Name.name
  email = "alina-#{n+1}@rails.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end
