require 'test_helper'
require 'pry'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:alina)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: {  name:                   "",
                                    email:                   "user@use.com",
                                    password:                "fod",
                                    password_confirmation:   "good" }
    assert_template 'users/edit'
  end

  test "successful edit with frindly forwarding" do
    get edit_user_path(@user)
    assert_not_nil session[:forwarding_url]
    log_in_as(@user)
    #binding.pry
    assert_redirected_to edit_user_path(@user)
    name =  "Foo bar"
    email = "foo@bar.com"
    patch user_path(@user), user: {  name:                   name,
                                    email:                   email,
                                    password:                "",
                                    password_confirmation:   "" }
    assert_not flash.empty?
    assert_redirected_to @user
    assert_nil session[:forwarding_url]
    @user.reload
    assert_equal @user.name, name
    assert_equal @user.email, email
  end
end
