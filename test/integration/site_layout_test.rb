require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  test "layout links" do
      get root_path
      assert_template 'static_pages/home'
      assert_select "a[href=?]", root_path, count: 2
      assert_select "a[href=?]", about_path
      assert_select "a[href=?]", help_path
      assert_select "a[href=?]", contact_path
      assert_select "a[hraf=?]", login_path
      user = users(:alina)
      log_in_as(user)
      get root_path
      assert_select "a[hraf=?]", logout_path
      assert_select "a[hraf=?]", users_path
      assert_select "a[hraf=?]", user_path(user)
      assert_select "a[hraf=?]", edit_user_path
  end
end
